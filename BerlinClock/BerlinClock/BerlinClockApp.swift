//
//  BerlinClockApp.swift
//  BerlinClock
//
//  Created by bmukandiwa on 25/1/2024.
//

import SwiftUI

@main
struct BerlinClockApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
